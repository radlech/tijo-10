import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfGenerator {

    private final static String FILE = "Y:\\resume.pdf";
    private static BaseFont times;
    private static Font headerFont = new Font(times, 24, Font.BOLD);
    private static Font tableFont = new Font(times, 12, Font.NORMAL);

    static {
        try {
            times = BaseFont.createFont("assets/fonts/times.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addContent(document);
            document.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addContent(Document document) throws DocumentException {

        document.addTitle("Resume");
        document.addAuthor("Radosław Lechowicz");
        document.addCreator("Radosław Lechowicz");

        Paragraph preface = new Paragraph();
        preface.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Resume",headerFont));
        addEmptyLine(preface, 3);
        document.add(preface);

        PdfPTable table = new PdfPTable(2);
        table.addCell(new PdfPCell(new Phrase("First name",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Radoslaw",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Last name",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Lechowicz",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Profession",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Student",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Education",tableFont)));
        table.addCell(new PdfPCell(new Phrase("2015 - 2019 PWSZ Tarnów \n2011 - 2015 ZSME Tarnów",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Summary",tableFont)));
        table.addCell(new PdfPCell(new Phrase("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Phasellus sit amet ex lacinia, tincidunt tellus vel, semper purus. Vestibulum id purus at elit " +
                "suscipit mattis. Etiam laoreet imperdiet risus. Ut laoreet ligula at turpis egestas egestas.",tableFont)));
        document.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i=0; i<number; i++) {
            paragraph.add(new Paragraph(""));
        }
    }
}